FROM node:14

COPY source /source

WORKDIR /source

RUN npm install

EXPOSE 4200

CMD ["npm", "run", "start", "--", "--host", "0.0.0.0", "--disable-host-check"]
